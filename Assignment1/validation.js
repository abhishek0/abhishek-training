// Write a program that shows a form on web browser, with fields email, password, sex (drop down with option of male, female, other), Role ( Radio button with option of Admin and User), Permissions (Checkbox with option Perm1, Perm2, Perm3, Perm4), Submit button.

// On clicking Submit button, following validation should take place
// 1. All fields should be filled. 
// 2. Email should be valid
// 3. password should be min 6 character with MIX of Uppercase, lowercase, digits
// 4. Atleast 2 permissions should be ticked.

// If all validation pass, the form and submit button should disapper and all the filled in details in the form should appear with a Confirm Button.

let errorElement = document.getElementById("error");
let email  		 = document.getElementById('email');
let pass   		 = document.getElementById('pass');
let gender 		 = document.getElementById('drop');
let role   		 = document.getElementsByName('role');
let perms  		 = document.getElementsByName('Perm');
let submit       = document.getElementById("submit");
let form 		 = document.test;

form.addEventListener("submit", (e) => {
	let passValidate = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/;
	let messages = [];
	let checkPerms = [];

	for(let perm of perms) {
		checkPerms.push(perm.checked);
	}

	// Email Validation
	if(email.value.split("@")[1].split(".").length === 1) {
		messages.push(`(${messages.length+1}) Invalid E-mail.`);
	}

	// Password Validation
	if(pass.value.match(passValidate) === null) {
		messages.push(`(${messages.length+1}) Password should be min 6 character with MIX of Uppercase, lowercase, digits.`);
	}

	// Checking whethe gender is selected or not
	if(gender.value === "-1") {
		messages.push(`(${messages.length+1}) Select a gender.`);
	}

	// Permission validation
	if(checkPerms.filter((a) => a === true).length < 2) {
		messages.push(`(${messages.length+1}) Atleast 2 permissions should be ticked`);
	}

	// Displaying error messages
	if(messages.length > 0) {
		errorElement.innerText = messages.join("\n");
		e.preventDefault();
	}
	else {
		// form.style.display = "none";
		submit.value = "Confirm";
		submit.style.background = "green";
		e.preventDefault();
	}
});
