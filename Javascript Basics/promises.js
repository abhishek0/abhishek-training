// Promises
const articles = [
	{
		title:"Article 1",
		author:"Whoever",
		body:"This is an article by Whoever"
	},
	{
		title:"Article 2",
		author:"Whatever",
		body:"This is an article by Whatever"
	},
	{
		title:"Article 3",
		author:"Everyone",
		body:"This is an article by Everyone"
	}
];

let getArticles = function() {
	setTimeout(() => {
		let res = "";
		articles.forEach((article) => {
			res += `<li>${article.title}</li>`;
			document.body.innerHTML = res;
		});
	}, 1000);
}

let createArticle = function(article) {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			articles.push(article);

			const error = true;

			if(!error) {
				resolve();
			}
			else {
				reject("Error. Something went wrong!!!!");
			}
		}, 2000);
	});
};

createArticle({title: "Article 4", author: "No-one", body: "This is an article by No-one"})
	.then(getArticles)
	.catch(err => console.log(err));


// Promises.all // returns an array of the results of the promises.
let promise1 = Promise.resolve("Hello World");
let promise2 = 44;
let promise3 = new Promise((resolve, reject) => setTimeout(resolve, 2000, "Goodbye"));

let promise4 = fetch('https://jsonplaceholder.typicode.com/users')
			   .then(res => res.json());

Promise.all([promise1, promise2, promise3, promise4])
	.then(values => console.log(values));
