// Callbacks
const articles = [
	{
		title:"Article 1",
		author:"Whoever",
		body:"This is an article by Whoever"
	},
	{
		title:"Article 2",
		author:"Whatever",
		body:"This is an article by Whatever"
	},
	{
		title:"Article 3",
		author:"Everyone",
		body:"This is an article by Everyone"
	}
];

let getArticles = function() {
	setTimeout(() => {
		let res = "";
		articles.forEach((article) => {
			res += `<li>${article.title}</li>`;
			document.body.innerHTML = res;
		});
	}, 1000);
}

let createArticle = function(article, callback) {
	setTimeout(() => {
		articles.push(article);
		callback();
	}, 2000);
};

createArticle({title: "Article 4", author: "No-one", body: "This is an article by No-one"}, getArticles);
