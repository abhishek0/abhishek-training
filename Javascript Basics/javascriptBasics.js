// (1) Basic syntax
// (1.1) Variables and types (using var(function scope), let(block scope), const(cannot change))

// Hoisting // console.log(a); var and function declarations are moved to the top.
var a = 1;
var b = "sample String";
var c = 3.53;

let a1 = 1;
let b1 = "string";
let c1 = 3.24;

const a2 = 23;
const b2 = "ssssss";
const c2 = 324.234;

// console.log(10-"10"); Automatic type casting.
// var x = 10/0 --->Infinity;
// console.log();

// console.log(a, a1, a2); // Allows you to log things in the console
// console.log(b, b1, b2);
// console.log(c, c1, c2);

// a2 = 44; // Cannot re-initialize a const variable

// console.log(b.length, b1.length, b2.length);
// console.log()
// Strings are Immutable.
// b[0] = "Z"; // No Change
// console.log(b); --> "sample String"


// Arrays in Javascript
// Note:- When an array is declared using the const keyword it cannot be reassigned with any other array but operations can be performed on that array.
var arr = [1,2,3,4,5,6,7,8,9]; // Can be of any datatype and can be either heterogenous or homogenous and can be nested as well
arr[8] = 99;
// console.log(arr);
// Arrays.push() 
// arr.push(55,22); // adds an element to the right of the array.
// Arrays.pop();
// arr.pop() // removes the rightmost element of the array. Returns the removed element.
// Arrays.shift()
// arr.shift() // removes the left most element of the array. Returns the removed element.
// Arrays.unshift()
// arr.unshift(0); // adds an element to the left of the array.

// Loops in javascript

// for loop
// for(let i = 0; i < 10; i++) {
// 	console.log(i);
// }

// for(let i = 0; i < arr.length; i++) {
// 	console.log(arr[i]);
// }

// for..in loop (Iterates through properties of an object)
var dict = {
	'a':1,
	'b':2,
	'c':3,
	'd':4,
	'e':5
}
// for (var i in dict) {
// 	console.log(i);
// }

// for..of loop (Lets you iterate through any iterable data structure)
// for(var i of b) {
// 	console.log(i);
// }

// while loop (Entry controlled)
let n = 10;
// while(n-->0) {
// 	console.log(n);
// }

// do..while loop (Exit controlled)

// do {
// 	console.log(n)
// } while(n-->0);

// If else and switch cases in javascipt.
// if(n%2 == 0) {
// 	console.log("even");
// }
// else {
// 	console.log("odd");
// }
var count = 0;
var sizeOfNumber = function(n) {
	if(n < 5) {
		return "Tiny";
	}
	else if(n < 10) {
		return "Small";
	}
	else if(n < 15) {
		return "Medium";
	}
	else if(n < 20) {
		return "Large";
	}
	else {
		return "Huge";
	}
}

var vowel = function() {
	console.log("Vowel");
}

var char = "f";

// switch(char.toUpperCase()) {
// 	case 'A':
// 	case 'E':
// 	case 'I':
// 	case 'O':
// 	case 'U':   vowel();
// 				break;

// 	default:    console.log("Consonant");
// }

// Object in javascript
var furniture = {
	"name": "table",
	"price": 5000,
	"material": ["wood","glass"],
	"owner": "None"
};

// Accessing properties of an object.
// Dot Notation
// console.log(furniture.name);
// console.log(furniture.price);
// Bracket Notation (Used when the property name contains space(s))
// console.log(furniture["material"], furniture["owner"]);

// Updating object properties
// Dot notation
furniture.price = 4999;
// console.log(furniture.price);

// Adding new properties to objects
furniture.company = "farniture";
furniture["Manufacturing date"] = "29-05-2017";
// console.log(furniture["company"]);

// Deleting properties from objects
// delete furniture.company;
// console.log(furniture.company);

// Testing objects for properties. <Object>.hasOwnProperty(<propertyName>)
// console.log(furniture.hasOwnProperty("owner"));

// Manipulating complex objects
var collection = {
	"1234": {
		"album": "Notes From the Underground",
		"artist": "Hollywood Undead",
		"tracks": [
			"pigskin",
			"lion",
			"Rain",
			"Outside"
		]
	},
	"1423": {
		"album": "Swan Songs",
		"artist": "Hollywood Undead",
		"tracks": [
			"Undead",
			"Young",
			"Everywhere I Go"
		]
	},
	"1546": {
		"artist": "Linkin Park",
		"tracks": [
			"Given Up",
			"In the End",
			"Numb",
			"Rebellion"
		]
	},
	"2345": {
		"album": "Three Cheers for Sweet Revenge"
	}
};

var collectionCopy = JSON.parse(JSON.stringify(collection)); // Used to create a shallow copy of the collecton object.

var updateRecord = function(id, prop, value) {
	if(value === "") {
		delete collection[id][prop];
	}
	else if(prop === "tracks") {
		collection[id][prop] = collection[id][prop] || []; // If the property already exists then no change, else the property is set to an empty array (Used the concept of undefined || <Anything> ==> <Anything>)
		collection[id][prop].push(value);
	}
	else {
		collection[id][prop] = value;
	}

	return collection;
}

var contacts = [
	{
		"firstname": "Ram",
		"lastname": "Pal",
		"number": "543985934",
		"likes": ["Chess", "Table Tennis"]
	},
	{
		"firstname": "Shyam",
		"lastname": "Singh",
		"number": "6743985934",
		"likes": ["Anime"]
	},
	{
		"firstname": "Harit",
		"lastname": "Sharma",
		"number": "unknown",
		"likes": ["Football", "Carom Board"]
	},
	{
		"firstname": "Birbal",
		"lastname": "Chaurasia",
		"number": "867982934",
		"likes": ["Cricket", "Movies"]
	}
];

var lookupProfile = function(name, prop) {
	for (var profile of contacts) {
		if(profile.firstname === name) {
			return profile[prop] || "No such property";
		}
	}
	return "No such contact";
};


// Math.random(); generates random numbers between 0 and 1
var randomRange = function(min, max) {
	return Math.floor(Math.random()*(max-min+1))+min;
};

// parseInt()
var strToInt = (s => parseInt(s)); // use of arrow function and the parseInt function.
// It can also be used for base conversions.
var binaryStringToInt = (s => parseInt(s, 2)); // converts a binary string into an Integer.

// Ternary operator
var oddEven = (n => (n%2 === 0)?"Even":"Odd");

// Preventing object mutation using Object.freeze (Does not work on nested objects).
const MATH_CONSTANTS = {
	PI: 3.141,
	ARR: [1,2,3]
};
Object.freeze(MATH_CONSTANTS);

MATH_CONSTANTS.PI = 234; // No change on the object because of Object.freeze();
MATH_CONSTANTS.ARR[0] = 5; // ARR becomes [5,2,3] since Object.freeze() does not work on nested objects.
// console.log(MATH_CONSTANTS.ARR);
// console.log(MATH_CONSTANTS.PI);

// Arrow functions and Anonymous functions
// Anonymous functions do not have a name the can be assigned to a variable and then can be called by that variable name.
var square = function(n) {
	return n*n;
}; // Anonymous function

// This can be improved by using arrow function.
var cube = (n) => n*n*n;
var concat = (arr1, arr2) => arr1.concat(arr2);

// Default parameters in javascript
const increment = (a, b = 1) => a+b;

// Spread and rest operator // When using it as a functional argument it is called the rest operator else it is called the spread operator.
const sum = (...arr) => {
	const inp = [...arr];
	return inp.reduce((a,b) => a+b);
};
// Spread operator can also be used to create a shallow copy of an iterable object.
// console.log(...arr)

// Destructuring Assignment to assign variables from objects.
let vari = {x: 1.2, y: 4.5, z: 8.9}

// The old way
// let x = vari.x;
// let y = vari.y;
// let z = vari.z;

// Using destructuring assignment
let {x:x, y:y, z:z} = vari;
// console.log(x, y, z);

// using destructuring on nested objects
let temperature = {
	today: { min:77 , max:84 },
	tomorrow: { min:79 , max:89 }
}

// Find max temperature of today and tommorow
let { today: { max:maximum }, tomorrow: {max: maximum1}} = temperature;
// console.log(maximum, maximum1);

// Destructuring to assign variables from arrays
const [temp1, temp2, , , temp3] = [1,2,3,4,5,6]; // temp1 = 1, temp2 = 2, temp3 = 5
// console.log(temp1, temp2, temp3);

// Swapping using destrcturing
var a = 1, b = 2;
[a, b] = [b, a];
// console.log(a, b);

// Destructuring assignment with the rest operator.
const removeFirstThree = function(list) {
	const [, , , ...tempo] = list;
	return tempo;
}

// Using destructuring assignment to pass an Object as a Function's parameter (Generally used in case of api calls, used to filter the data that we don't need).
const stats = {
	max: 56.78,
	standardDeviation: 4.34,
	median: 34.54,
	mode: 23.87,
	min: -0.75,
	average: 35.85
}

// without Destructuring
var half = function(stats) {
	return (stats.min+stats.max)/2;
}

// With destructuring 
var half2 = function({max, min}) {
	return (max+min)/2;
}

// Using template literals
let namee = "Kanzaki";
let age = 35;

let info = `${namee}-san is ${age} years old, and is a great samurai.`
// console.log(info);


const result = {
	success: ["max-length", "no-amd", "prefer-arrow-function"],
	failure: ["no-var", "var-on-top", "linebreak"],
	skipped: ["id-blacklist", "no-dup-keys"]
}

const makeList = function(arr) {
	const resultDisplayArray = [];
	for(let i of arr) {
		let tempStr = `<li class ="text-warning">${i}</li>`
		resultDisplayArray.push(tempStr);
	}
	return resultDisplayArray;
}


// Old way
const createPerson = (name, age, gender) => {
	return {
		name: name,
		age: age,
		gender: gender
	} 
}
// New way
const createPerson1 = (name, age, gender) => ({name, age, gender});

// Functions inside of objects
// Old way
const objact = {
	name: "whatever",
	printname: function() {
		console.log(this.name);
	}
};
// New way
const obaject = {
	name: "whatever be the weather be",
	printname() {
		console.log(this.name);
	}
};


// Classes in javascript
// (1) using Functions
// const Fruit = function(name, color) {
// 	this.name = name;
// 	this.color = color;
// }

// const Orange = new Fruit("Orange", "orange");
// console.log(Orange.name);
// (2) using class keyword
class Fruit {
	constructor(name, color) {
		this.name = name;
		this.color = color;
	}
};

const Apple = new Fruit("Apple", "red");
const Orange = new Fruit("Orange", "orange");
// console.log(Apple.color, Orange.color);

// Using getters and setters to control access to an object
class Marks {
	constructor(maths, english, science) {
		this._maths = maths;
		this._english = english;
		this._science = science;
	}

	// Getters
	get mathsMarks() {
		return this._maths;
	}
	get englishMarks() {
		return this._english;
	}
	get scienceMarks() {
		return this._science;
	}

	// Setters
	set mathsMarks(marks) {
		this._maths = marks;
	}
	set englishMarks(marks) {
		this._english = marks;
	}
	set scienceMarks(marks) {
		this._science = marks;
	}
}

class Book {
	constructor(author) {
		this._author = author;
	}

	get writer() {
		return this._author;
	}

	set writer(author) {
		this._author = author;
	}
}

// Javascript Prototype
// Whenever a variable, array, function or an object is created in javascript, the javascript engine bind an object __proto__ to it
// This object is also known as dunder proto, this dunder proto contains various functions and properties which can be used 
// by the variable, array, function or the object created using the dot operator.
class Vehicle {
	constructor(model, color) {
		this.model = model;
		this.color = color;
		this.returnDetail = function() {
			return `The ${this.model} is ${this.color}`;
		}
	}

	printShit() {
		console.log("Print SHITTTTTTTTTT!!!!!!!!!")
	}
}

Vehicle.prototype.newStuff = function() {
	console.log("New Stuff");
}

let savari = new Vehicle("Buggi", "black");
// console.log(Vehicle.prototype === savari.__proto__);
// console.log(savari.newStuff());

// IIFEs (anything declared within IIFE's cannot be accessed outside of it.)
let arrrr = [1,2,3,4,5,6,7];
// (function() {
// 	let sum = 0;
// 	for (let i of arrrr) {
// 		sum += i;
// 	}
// 	console.log(sum);
// })();

// Javascript closures.
// A closure is an inner function that has access to the outer(enclosing) function's variables - scope chains.
// A closure has three scope chains: it has access to its own scope, it has access to the outer function's scope and it has access to the global scope.
// A closure is a function with preserved data.
// Example 1
function addN(n) {
	var res = function(num) {
		return n+num;
	}
	return res;
}

var add3 = addN(3);
var add4 = addN(4);

// console.log(add3(5));
// console.log(add4(5));

function greeting(nameo) {
	var displayName = function(abc) {
		return abc + " " + nameo;
	}
	return displayName;
}

var what = greeting("Ram");
var why  = greeting("Raju");

// console.log(what("hello"));
// console.log(why("hello"));


// Event handlers --> It is a function that runs when an event fires.
// let textChange = () => {
// 	const p = document.querySelector('p');
// 	p.textContent = (p.textContent === "Changed")?"Click to change!!":"Changed";
// };
// let textAlert = () => {
// 	alert("What am i supposed to do?")
// }

// Adding an event handler as a property of the button element
// const click = document.querySelector('button');
// click.onclick = textChange;


// Event Listener --> It adds responsiveness to an element, which allows the element to wait or "listen" for the given event to fire.
// Listens for an event to occour rather than than binding it to the property of an element.
// Takes 2 mandatory parameters --> (1) the event to listen (2) The listener callback function.
// Multiple listener can be added to the same event.
// const button = document.querySelector('button');
// button.addEventListener('click', () => {
// 	const p = document.querySelector('p');
// 	p.textContent = (p.textContent === "Changed")?"Click to change!!":"Changed";
// });

// button.addEventListener('click', () => {
// 	alert("What am i supposed to do?")
// });

// let para = document.getElementById("para");
// document.addEventListener('keydown', (ev) => {
// 	console.log(ev.key);
// 	switch(ev.key.toLowerCase()) {
// 		case 'a': para.textContent = "LEFT";
// 				  break;
// 		case 's': para.textContent = "DOWN";
// 				  break;
// 		case 'd': para.textContent = "RIGHT";
// 				  break;
// 		case 'w': para.textContent = "UP";
// 				  break;
// 	}
// });

// document.addEventListener('click', (ev) => {
// 	console.log(ev)
// 	console.log(ev.target);
// });

// Important Events
// (1) click      --> (fires when the left mouse button is pressed once)
// (2) dblclick   --> (fires when the left mouse button is pressed twice)
// (3) mouseenter --> (fires when the mouse pointer enters an element)
// (4) mouseleave --> (fires when the mouse pointer leaves an element)
// (5) mousemove  --> (fires every time the mouse pointer moves inside an element)

// HTML Form Events
// (1) Input - Submit
// (2) Change - Focus - Blur
// (3) Click - MouseDown - MouseUp
// (4) KeyDown - KeyUp - KeyPress

// 
document.addEventListener('DOMContentLoaded', () => {
	let name = document.getElementById('name');
	let email = document.getElementById('email');
	let password = document.getElementById('pass');
	let phone = document.getElementById('phone');

	// name.addEventListener("focus", () => {
	// 	name.style.backgroundColor = "#ff8080"
	// });
	// name.addEventListener('blur', () => {
	// 	name.style.backgroundColor = null
	// });

	// email.addEventListener("focus", () => {
	// 	email.style.backgroundColor = "#ff8080"
	// });
	// email.addEventListener('blur', () => {
	// 	email.style.backgroundColor = null
	// });

	// password.addEventListener("focus", () => {
	// 	password.style.backgroundColor = "#ff8080"
	// });
	// password.addEventListener('blur', () => {
	// 	password.style.backgroundColor = null
	// });

	// phone.addEventListener("focus", () => {
	// 	phone.style.backgroundColor = "#ff8080"
	// });
	// phone.addEventListener('blur', () => {
	// 	phone.style.backgroundColor = null
	// });
});


// Event Objects
// The event object contains methods and properties that all events can access.
// It is passed to the listener function as a parameter.
// document.addEventListener('mouseup', (ev) => {
// 	console.log(ev.target);
// });

// Javascript Event Loop
// Call stack, event loop, callback queue, apis and stuff
// settimeout DOM AJAX are not in the v8 source code. These things are present in the web apis and are extra things that the browser provides.
// Single thread --> One thing at a time
// Call Stack --> data structure records where in the program we are.
// Blocking --> Code that is slow. Since the code is slow it takes time and due to js being single threaded the rest of the code execution have to wait for it to finish.
// The js runtime can do one thing at a time. The web apis we talked about earlier are some really complex c++(multi threaded) code that runs in the background.

// console.log("Hello Bahrld"); // This goes to the call stack (Executes simply since it is just a console.log statement and does not take much time).
// setTimeout(() => {
	// console.log("Doin some stuff");   // This goes to the call stack and the web apis in the browser are called and a timer of 5 minutes is started
// }, 5000);							 //  Now since the setTimeout call itself is not complete it is popped off the stack.
// console.log("What the heck is happening over here?"); // Now this statemet gets pushed onto the stack and gets executed and gets popped off the stack.

// Now the web api can't just jump right back onto the stack after the timer is complete. Now when this web api is done it adds the callback function onto the task queue
// Now finally come onto the event loop. The job of event loop is to check the task/ event queue and push its first element onto the stack.
// So the event loop picks our callback function and pushes it onto the call stack now this callback function is executed and removed from the call stack.

// Now in case when the setTimeout is 0 then the webapi gets called and the setTimeout is executed immediately (since the timer is set to zero) and gets 
// sent to the event/task queue. Now before the call stack is empty the event loop cannot push the callback function in the task queue back onto the stack 
// hence it waits for the call stack to be cleared and then pushed the callback onto the stack. So we get to see the same stuff happening again.

// Callback Hell Example
// Callbacks are functions that are passed to other functions. When the first function is done it calls the second function
let printString = (strInput, callback) => {
	setTimeout(
		() => {
			console.log(strInput);
			callback();
		},
	Math.floor(Math.random()*100) + 1);
};

// let printAll = () => {
// 	printString("A");
// 	printString("B");
// 	printString("C");
// }


// Callback Hell.
let printAll = () => {
	printString("A", () => {
		printString("B", () => {
			printString("C", () => {})
		})
	});
} // Complex shit.
