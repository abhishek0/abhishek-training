// Arrays in Javascript
// Note:- When an array is declared using the const keyword it cannot be reassigned with any other array but operations can be performed on that array.
var arr = [1,2,3,4,5,6,7,8,9]; // Can be of any datatype and can be either heterogenous or homogenous and can be nested as well

arr[8] = 99;
console.log(arr);

// Array operations
// Arrays.push() 
arr.push(55,22); // adds an element to the right of the array.

// Arrays.pop();
arr.pop() // removes the rightmost element of the array. Returns the removed element.

// Arrays.shift()
arr.shift() // removes the left most element of the array. Returns the removed element.

// Arrays.unshift()
arr.unshift(0); // adds an element to the left of the array.

