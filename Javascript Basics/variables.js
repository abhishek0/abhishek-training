// Variables and types (using var(function scope), let(block scope), const(cannot change))
// Hoisting --> var and function declarations are moved to the top.
console.log(a); // prints undefined (No error).
var a = 1;
var b = "sample String";
var c = 3.53;

let a1 = 1;
let b1 = "string";
let c1 = 3.24;

const a2 = 23;
const b2 = "ssssss";
const c2 = 324.234;

console.log(10-"10");  //  Automatic type casting.
var x = 10/0 // --->Infinity;

console.log(a, a1, a2); // Allows you to log things in the console
console.log(b, b1, b2);
console.log(c, c1, c2);

a2 = 44; // Cannot re-initialize a const variable


// Strings are Immutable.
b[0] = "Z"; // No Change
console.log(b); // ---> "sample String"
