// Async/Await 
const articles = [
	{
		title:"Article 1",
		author:"Whoever",
		body:"This is an article by Whoever"
	},
	{
		title:"Article 2",
		author:"Whatever",
		body:"This is an article by Whatever"
	},
	{
		title:"Article 3",
		author:"Everyone",
		body:"This is an article by Everyone"
	}
];


let createArticle = function(article) {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			articles.push(article);

			const error = true;

			if(!error) {
				resolve();
			}
			else {
				reject("Error. Something went wrong!!!!");
			}
		}, 2000);
	});
};


let getArticles = function() {
	setTimeout(() => {
		let res = "";
		articles.forEach((article) => {
			res += `<li>${article.title}</li>`;
			document.body.innerHTML = res;
		});
	}, 1000);
}


async function init() {
	 // waits for an asyncronous process to complete before moving forward.
	await createArticle({title:'Article 4', author:'No-one', body: 'This in an article by No-one'});
	getArticles();
}

// init();

// Using async await with fetch.

async function fetchUsers() {
	const res = await fetch('https://jsonplaceholder.typicode.com/users').then(ans => ans.json());

	console.log(res);
}

// fetchUsers();
