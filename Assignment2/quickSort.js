// Write a Program that should contains 
// 1. Class Person with fields (name, age, salary, sex)
// 2. a static sort function in the class that should take array of Persons and name of the field and order of sorting and should return a new sorted array based on above inputs. Should not change intial array.

// for example Person.sort(arr, 'name', 'asc') -> sort array of persons based on name in ascending order. 'desc' for descending

// 3. You have to write Quick sort for this sorting algorithms.

class Person {
	constructor(name, age, salary, gender) {
		this.name = name;
		this.age = age;
		this.salary = salary;
		this.gender = gender;
	}

	// Return the index of the leftmost element in the sorted array.
	static partition(arr, low, high, field, order) {
		let pivot = arr[high][field];
		let i = low-1;

		for(let j = low; j < high; j++) {
			if(order === 'desc') {
				if(arr[j][field] > pivot) {
					i++;
					// Swapping arr[i] and arr[j]
					[arr[i], arr[j]] = [arr[j], arr[i]];
				}
			}
			else {
				if(arr[j][field] < pivot) {
					i++;
					// Swapping arr[i] and arr[j]
					[arr[i], arr[j]] = [arr[j], arr[i]];
				}	
			}
		}
		// Swapping arr[i+1] and arr[high]
		[arr[i+1], arr[high]] = [arr[high], arr[i+1]];
		return i+1;
	}

	// Quick Sort function
	static quickSort(arr, low, high, field, order) {
		if(low < high) {
			let pivot = this.partition(arr, low, high, field, order);
			this.quickSort(arr, low, pivot-1, field, order);
			this.quickSort(arr, pivot+1, high, field, order);
		}
	}

	// Our static sort function
	static sort(arr, field, order) {
		this.quickSort([...arr], 0, arr.length-1, field, order);
	}
}

let personArr = [
	new Person("Kakashi", 27, 70000, 'M'),
	new Person("Sakura", 20, 15000, 'F'),
	new Person("Sugimoto", 23, 70000, 'M'),
	new Person("Female4", 20, 50000, 'F'),
	new Person("Zaraki", 21, 50000, 'M'),
	new Person("Female2", 20, 45000, 'F'),
	new Person("Shinra", 22, 25000, 'M'),
	new Person("Female", 18, 35000, 'F'),
	new Person("Roshi", 77, 65000, 'M'),
];

console.log(Person.sort(personArr, 'age', 'desc'));
